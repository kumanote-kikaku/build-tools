(function() {
  'use strict';

  /* eslint no-console: "off" */

  function setupTools(gulp, dest) {
    const src = require('path').dirname(__filename);
    const logger = require('./builder/scripts/plugins/logger.js');

    gulp.task('setup-tools', () => {
      return gulp.src([
        `${src}/.eslintrc`,
        `${src}/.csscomb.json`,
        `${src}/.gitignore`,
        `${src}/setting.js`])
        .pipe(gulp.dest(dest))
        .pipe(logger('setup', dest));
    });
  }

  function updateTools(gulp, dest) {
    const src = require('path').dirname(__filename);
    const logger = require('./builder/scripts/plugins/logger.js');

    gulp.task('update-tools', () => {
      return gulp.src([
        `${src}/gulpfile.js`,
        `${src}/file*/**/*.*`])
        .pipe(gulp.dest(dest))
        .pipe(logger('update', dest));
    });
  }

  function start(gulp, setting, dest) {
    for (let task in setting.task) {
      require(`./builder/scripts/tasks/${task}`)(gulp, setting);
    }

    require('./builder/scripts/tasks/common')(gulp, setting);

    if ('serve' in setting) {
      switch (setting.serve.type) {
      case 'php':
        require('./builder/scripts/tasks/serve-php')(gulp, setting);
        break;
      case 'node':
        require('./builder/scripts/tasks/serve-node')(gulp, setting);
        break;
      }
    } else {
      require(`./builder/scripts/tasks/no-serve`)(gulp, setting);
    }
    updateTools(gulp, dest);
  }

  function setup(gulp, dest) {
    setupTools(gulp, dest);
    updateTools(gulp, dest);

    gulp.task('default', ['setup-tools', 'update-tools'], (cb) => {
      console.log();
      console.log('build-tools setuped.');
      console.log('next step:');
      console.log('  1: edit `setting.js\'');
      console.log('  2: run `gulp\'');
      console.log();
      cb();
    });
  }

  module.exports = {
    start: start,
    setup: setup
  };
})();
