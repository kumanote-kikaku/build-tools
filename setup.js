(function() {
  'use strict';

  /* eslint no-console: "off" */

  const path = require('path');
  const fs = require('fs');

  const tooldir = path.dirname(path.resolve(__filename));
  const rootdir = path.resolve('.');

  const dest = path.join(rootdir, 'gulpfile.js');
  const setting = path.join(rootdir, 'setting.js');

  let hasGulpfile = true;
  let isSetuped = true;

  try { fs.statSync(dest); } catch (er) { hasGulpfile = false; }
  try { fs.statSync(setting); } catch (er) { isSetuped = false; }

  fs.createReadStream(path.join(tooldir, 'gulpfile.js'))
    .pipe(fs.createWriteStream(path.join(rootdir, 'gulpfile.js')).on('close', function() {
      console.log((hasGulpfile ? 'overwrite' : 'create') + ' `gulpfule.js\' in current directory.');
      if (isSetuped) {
        console.log('run `gulp update-tools\' to update build-tools.');
      } else {
        console.log('run `gulp\' to setup.');
      }
    }));
})();
