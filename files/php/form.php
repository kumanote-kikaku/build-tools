<?php // -*- indent-tabs-mode: nil; mode: php; -*-

/* ========================================================================= */
/* 設定 */

/* ------------------------------------------------------------------------- */
/* phpの設定 */

// 文字コード設定
mb_language('Japanese');
mb_internal_encoding('UTF-8');

/* ------------------------------------------------------------------------- */
/* 返信メールの設定 */

/*
  メールアドレスは以下のような形式が使用可能

  単純な指定
  'foo@example.com'

  表示名とアドレスを指定（make_addressを使う）
  make_address('名前', 'bar@example.com')

  複数指定（arrayにする）
  array('foo@example.com', make_address('名前', 'bar@example.com')
*/

// 返信メールの送信先メールアドレス
function config_reply_to($data) {
  return null;
}

// 返信メールのfromに記載するメールアドレス
function config_reply_from($data) {
  return null;
}

// 返信メールのCC送信先メールアドレス
function config_reply_cc($data) {
  return null;
}

// 返信メールのBCC送信先メールアドレス
function config_reply_bcc($data) {
  return null;
}

// 返信メールの件名
function config_reply_subject($data) {
  return null;
}

// 返信メールの内容
function config_reply_body($data) {
  return <<<__EOT
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
__EOT;
}

/* ------------------------------------------------------------------------- */
/* 通知メールの設定 */

// 通知メールの送信先メールアドレス
function config_notify_to($data) {
  return null;
}

// 通知メールのFromに記載するメールアドレス
function config_notify_from($data) {
}

// 通知メールのCC送信先メールアドレス
function config_notify_cc($data) {
  return null;
}

// 通知メールのBCC送信先メールアドレス
function config_notify_bcc($data) {
  return null;
}

// 通知メールの件名
function config_notify_subject($data) {
  return null;
}

// 通知メールの内容
function config_notify_body($data) {
  return <<<__EOT
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
__EOT;
}

/* ========================================================================= */
/* ユーティリティ */

// $_POST[$name]の値の有無をチェック
function has_post($name) {
  if (!isset($_POST[$name])) return false;
  $value = $_POST[$name];
  if (is_array($value)) {
    return count($value) > 0;
  }
  return strlen($value) > 0;
}

// HTMLエスケープ
function esc_html($value) {
  return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
}

/* ------------------------------------------------------------------------- */
/* バリデーション */

// 空チェック
function check_not_empty($value) {
  return strlen($value) > 0;
}

// 選択値チェック
function check_selected($value, $items) {
  return in_array($value, $items, true);
}

// 簡易メールアドレスチェック
function check_mail($value) {
  return preg_match('/^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/', $value);
}

/* ------------------------------------------------------------------------- */
/* メール処理 */

// 名前付きのメールアドレスを作成
function make_address($name, $address) {
  return array('name' => $name, 'address' => $address);
}

// メールアドレスの正規化
function normalize_address($address) {

  // 単一指定
  if (is_string($address) && strlen($address) > 0 && check_mail($address)) {
    return $address;
  }

  if (!is_array($address)) {
    return null;
  }

  // make_addressで生成されたもの
  if (isset($address['name']) && isset($address['address'])) {
    $name = $address['name'];
    $address = normalize_address($address['address']);
    if (is_string($name) && strlen($name) > 0 && $address !== null) {
      return mb_encode_mimeheader($name, 'ISO-2022-JP', 'B', "\x0a") . '<' . $address . '>';
    }
    return null;
  }

  // アドレスのリスト
  $result = array();
  foreach ($address as $x) {
    $x = normalize_address($x);
    if ($x !== null) {
      $result[] = $x;
    }
  }
  return (count($result) > 0) ? implode(',', $result) : null;
}

// メール送信
function sendmail($to, $cc, $bcc, $from, $subject, $body) {
  $to = normalize_address($to);
  $from = normalize_address($from);
  $cc = normalize_address($cc);
  $bcc = normalize_address($bcc);
  $subject = trim(strval($subject));
  $body = trim(strval($body));
  $header = array();

  if ($to === null) {
    form_set_error('宛先が正しくありません。');
    return false;
  }

  if ($from === null) {
    form_set_error('送信元が正しくありません。');
    return false;
  }
  $header[] = 'From: ' . $from;

  if ($cc !== null) {
    $header[] = 'Cc: ' . $cc;
  }

  if ($bcc !== null) {
    $header[] = 'Bcc: ' . $bcc;
  }

  return mb_send_mail($to, $subject, $body, implode("\x0d\x0a", $header));
}

/* ========================================================================= */
/* フォーム */

$global_form = array(
  'mode' => 'form',
  'data' => array(),
  'error' => array(),
  'global_error' => array(),
);

/* ------------------------------------------------------------------------- */
/* 初期化 */

function form_init($fields=array()) {
  global $global_form;

  $action = 'initial';
  if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] === 'POST') {
    // input[type=hidden][name=action]からのaction推定
    if (isset($_POST['action'])) {
      switch ($_POST['action']) {
        case 'send':
        case 'edit':
        case 'check':
          $action = $_POST['action'];
          break;
      }
    } else {
      // ボタンからのaction推定
      if (has_post('action_send')) {
        $action = 'send';
      } else if (has_post('action_back')) {
        $action = 'edit';
      } else if (has_post('action_check')) {
        $action = 'check';
      }
    }
  }

  switch ($action) {
    case 'initial':
      $global_form['mode'] = 'form';
      break;

    case 'edit':
      form_pump($fields);
      $global_form['mode'] = 'form';
      break;

    case 'check':
      form_pump($fields);
      if (!form_check() ) {
        $global_form['mode'] = 'form';
      } else {
        $global_form['mode'] = 'confirm';
      }
      break;

    case 'send':
      form_pump($fields);
      if (!form_check() ) {
        $global_form['mode'] = 'form';
      } else if (!form_send()) {
        $global_form['mode'] = 'form';
      } else {
        $global_form['mode'] = 'complete';
      }
      break;
  }
}

/* ------------------------------------------------------------------------- */
/* チェック */

function form_check() {
  // 独自に実装する
  form_set_error('チェック処理が実装されていません。');
  return false;
}

/* ------------------------------------------------------------------------- */
/* 送信 */

function form_send() {
  // 独自に実装する
  form_set_error('送信処理が実装されていません。');
  return false;
}

/* ------------------------------------------------------------------------- */
/* モード判定 */

// モード判定
function form_mode_is($mode) {
  global $global_form;
  return ($global_form['mode'] === $mode);
}

// 該当するモードなら$valueを返す
function form_mode_value($mode, $value) {
  return (form_mode_is($mode) ? $value : '');
}

/* ------------------------------------------------------------------------- */
/* データの処理 */

// $_POSTからのくみ上げ
function form_pump($fields) {
  global $global_form;
  foreach ($fields as $name) {
    if (isset($_POST[$name])) {
      if (is_array($_POST[$name])) {
        $global_form['data'][$name] = $_POST[$name];
      } else {
        $global_form['data'][$name] = array($_POST[$name]);
      }
    }
  }
}

// データの取得
function form_get_all_value($name) {
  global $global_form;
  if (isset($global_form['data'][$name])) {
    return $global_form['data'][$name];
  }
  return array();
}

// データを一つ取得
function form_get_value($name) {
  $value = form_get_value_all($name);
  return (count($value) > 0) ? $value[0] : '';
}

/* ------------------------------------------------------------------------- */
/* エラーハンドリング */

// すべてのエラーの数を取得
function form_get_error_count() {
  global $global_form;
  $count = count($global_form['global_error']);
  foreach ($global_error['error'] as $name => $error) {
    $count += count($error);
  }
  return $count;
}

// 全体エラーの有無チェック
function form_has_error() {
  global $global_form;
  return count($global_form['global_error']) > 0;
}

// 全体エラーの設定
function form_set_error($message) {
  global $global_form;
  $global_form['global_error'][] = $message;
}

// 全体エラーの取得
function form_get_all_error() {
  global $global_form;
  return $global_form['global_error'];
}

// 全体エラーの一つ取得
function form_get_error($message) {
  $error = form_get_global_error();
  return (count($error) > 0) ? $error[0] : '';
}

// 指定フィールドのエラーの有無チェック
function form_has_field_error($name) {
  global $global_form;
  if (!isset($global_form['error'][$name])) return false;
  $error = $global_form['error'][$name];
  return is_array($error) && count($error) > 0;
}

// 指定フィールドのエラー設定
function form_set_field_error($name, $message) {
  global $global_form;
  if (!isset($global_form['error'][$name])) $global_form['error'][$name] = array();
  $global_form['error'][$name][] = $message;
}

// 指定フィールドのエラー取得
function form_get_all_field_error($name) {
  global $global_form;
  if (form_has_field_error($name)) {
    return $global_form['error'][$name];
  }
  return array();
}

// 指定フィールドのエラー一つ取得
function form_get_field_error($name) {
  $error = form_get_all_field_error($name);
  return (count($error) > 0) ? $error[0] : '';
}

/* ------------------------------------------------------------------------- */
/* デバッグ */

function form_debug() {
  global $global_form;

  var_dump(array(
    '_POST' => $_POST,
    'form' => $global_form,
  ));
}
