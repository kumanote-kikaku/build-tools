<?php
// Wordpress用のrouting

if (preg_match('/\.(?:png|jpg|jpeg|gif|css|js)$/', parse_url($_SERVER['REQUEST_URI'])['path'])) {
	return false;
}

$ds = DIRECTORY_SEPARATOR;
$preview = 'sites' . $ds . 'preview' . $ds . 'index.php';

$filename = $_SERVER['SCRIPT_FILENAME'];
if (!file_exists($filename)) {
	$filename = dirname(dirname(dirname(dirname(__FILE__)))) . $ds . $preview;
}
chdir(dirname($filename));
include $filename;
