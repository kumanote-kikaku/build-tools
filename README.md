# build tools

## 導入方法

### 1. gulpがglobalインストールされていない場合はインストール

    $ npm -g install gulp

※インストール済みの場合はスキップ

### 2. build-toolsをglobalインストール

    $ npm -g install https://bitbucket.org/kumanote-kikaku/build-tools.git

※インストール済みの場合はスキップ

### 3. プロジェクトのディレクトリを作成し移動

    $ mkdir my-project
    $ cd my-project

### 4. build-tools-setupを使用してgulpfile.jsを作成

    $ build-tools-setup

### 5. 初期データの作成

    $ gulp

※最初のgulp実行は、初期データの生成となる

### 6. ビルドプロセス開始

    $ gulp

※二回目移行のgulp実行は、ビルドプロセスの開始となる
