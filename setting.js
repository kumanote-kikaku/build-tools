// build-tools setting
module.exports = {

  // 設定フォーマット
  format: 0.4,

  // ディレクトリレイアウト
  directory: {
    src: 'sites/src',
    cache: 'sites/cache',
    preview: 'sites/preview',
    release: 'sites/release'
  },

  // Wordpressテーマの場合のディレクトリレイアウト
  // 使う場合は上の directory を _directory にし、こっちを directory にするなど
  _directory: {
    src: 'sites/src/wp-content/themes/THEME-NAME',
    cache: 'sites/cache/wp-content/themes/THEME-NAME',
    preview: 'sites/preview/wp-content/themes/THEME-NAME',
    release: 'sites/release/wp-content/themes/THEME-NAME'
  },

  // ファイル監視の設定
  watch: {
    // 間隔（ms）
    interval: 100
  },

  // 各タスクの設定
  task: {

    // 単純なコピー
    copy: {
      // 対象ファイルの拡張子
      exts: ['min.js', 'css', 'ico', 'pdf']
    },


    // include処理
    file: {
      // 対象ファイルの拡張子
      exts: ['html', 'php'],

      // ライブラリとして扱うファイルの接頭辞
      libraryPrefix: '_',

      // include時に追加するデータ
      appendix: {
        SITE_NAME: 'サイト名',
        BASE_URL: 'http://example.com/'
      }
    },


    // sassコンパイル
    sass: {
      // 対象ファイルの拡張子
      exts: ['sass', 'scss'],

      // ライブラリとして扱うファイルの接頭辞
      libraryPrefix: '_',

      // autoprefixerの対応ブラウザ指定
      browsers: ['> 5%', 'last 2 versions', 'Android 2.2', 'iOS 4', 'ie 9', 'Safari 5'],

      // media queriesをまとめるかどうか
      combineMq: false
    },


    // jsの整形
    js: {
      // 対象ファイルの拡張子
      exts: ['js'],

      // 非対象ファイルの拡張子
      ignoreExts: ['min.js']
    },


    // 画像最適化
    image: {
      // 対象ファイルの拡張子
      exts: ['gif', 'png', 'jpg', 'jpeg', 'svg']
    }

  },


  // 単純なローカルサーバの設定
  serve: {
    // 必ず指定する
    type: 'node',

    // ホストのIPアドレス
    host: '0.0.0.0',    // どこからでもアクセス可能
    _host: 'localhost', // 自分のみ

    // ホストのポート番号
    port: 8448,

    // ドキュメントルート・省略時は directory.preview
    _root: 'sites/preview',

    // ssiの使用
    ssi: false
  },


  // phpを使ったローカルサーバの設定
  _serve: {
    // 必ず指定する
    type: 'php',

    // ホストのIPアドレスとポート
    host: '0.0.0.0',
    port: 8448,

    // ドキュメントルート・省略時は directory.preview
    _root: 'sites/preview',

    // ルータスクリプトの指定
    _router: 'files/php/router/through.php',
    __router: 'files/php/router/for-wordpress.php',

    // php実行ファイルの場所
    // 最初に見つかったものが使用される
    phpExecutable: [
      '/usr/local/homebrew/bin/php',
      '/usr/local/bin/php',
      '/usr/bin/php',
      'c:/xampp/php/php.exe'],

    // php.iniの場所
    // 最初に見つかったものが使用されるが、プラットフォーム名がついているものが優先される
    phpIni: {
      default: ['files/php/ini/php.ini'],
      darwin: ['files/php/ini/php.darwin.ini'],
      win32: ['files/php/ini/php.win32.ini']
    }
  }

};
