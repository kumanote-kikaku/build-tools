(function() {
  'use strict';

  // /* RELEASE...<RELEASE>...RELEASE */
  // /* PREVIEW...<PREVIEW>...PREVIEW */

  const PATTERN = /((\/\*|<!--)\s*RELEASE\.\.\.|\.\.\.RELEASE\s*(\*\/|-->)|\/\*\s*PREVIEW\.\.\.(.|\x0a|\x0d)*?\.\.\.PREVIEW\s*\*\/|<!--\s*PREVIEW\.\.\.(.|\x0a|\x0d)*?\.\.\.PREVIEW\s*-->([\x0a\x0d]*))/mg;

  module.exports = function() {
    const replace = require('gulp-replace');
    return replace(PATTERN, '');
  };
})();
