(function() {
  'use strict';

  module.exports = function(label, baseDir, color) {
    const util = require('../util');
    const tap = require('gulp-tap');

    return tap(function(file) {
      util.log(label, file.path, baseDir, color);
    });
  };
})();
