(function() {
  'use strict';

  /* eslint no-console: "off" */

  const LOG_LABEL_SIZE = 19;
  const chalk = require('chalk');

  function colored(str, color) {
    if (typeof color === 'string') {
      try {
        return chalk[color].underline(str);
      } catch (er) {
      }
    }
    return str;
  }

  module.exports.ellipsis = function(str, length, replacement, style) {
    if (str.length < length) return str;

    const size = length - replacement.length;
    const left = size >> 1;
    const right = size - left;

    switch (style) {
    case 'start':
      return replacement + str.slice(-size);
    case 'center':
      return str.slice(0, left) + replacement + str.slice(-right);
    case 'end':
    default:
      return str.slice(0, size) + replacement;
    }
  };

  module.exports.log = function(label, path, baseDir, color) {
    module.exports.logMessage(label, module.exports.normalizePath(path, baseDir, true), color);
  };

  module.exports.logMessage = function(label, message, color) {
    let tmp;
    if (label.length < LOG_LABEL_SIZE) {
      tmp = `${' '.repeat(LOG_LABEL_SIZE)}${label}`.slice(-LOG_LABEL_SIZE);
    } else {
      tmp = module.exports.ellipsis(label, LOG_LABEL_SIZE, '...', 'start');
    }
    tmp = colored(tmp, color);
    console.log(`${tmp} : ${message}`);
  };

  module.exports.normalizePath = function(path, basePath, universal) {
    const node_path = require('path');

    let result = node_path.resolve(path);
    const base = node_path.resolve(basePath);
    if (result.indexOf(base) === 0) {
      result = result.slice(base.length);
    }
    if (universal) {
      return result.split(node_path.sep).join('/').replace(/^\/+/, '');
    }
    while (result.indexOf(node_path.sep) === 0) {
      result = result.slice(1);
    }
    return result;
  };

  module.exports.getString = function(value, fallback) {
    return (typeof value === 'string') ? value : fallback;
  };

  module.exports.getStringArray = function(value) {
    if (typeof value === 'string') return [value];
    if (typeof value === 'object' &&
        value !== null &&
        typeof value.length === 'number' &&
        isFinite(value.length)) {
      let array = [];
      for (let l = value.length, i = 0; i < l; ++i) {
        array = [].concat(array, module.exports.getStringArray(value[i]));
      }
      return array;
    }
    return [];
  };

  module.exports.getFilePattern = function(baseDir, filePrefix, exts) {
    const array = module.exports.getStringArray(exts);
    if (array.length === 0) {
      return `${baseDir}/**/${filePrefix}*`;
    }
    if (array.length === 1) {
      return `${baseDir}/**/${filePrefix}*.${array[0]}`;
    }
    return `${baseDir}/**/${filePrefix}*.{${array.join(',')}}`;
  };

  module.exports.getOldFiles = function(targetDir, sourceDir) {
    const fs = require('fs');
    const path = require('path');
    const target = path.resolve(targetDir);

    return new Promise((resolve) => {
      const globby = require('globby');
      const result = [];

      globby([path.join(target, '**', '*.*')]).then((paths) => {
        paths.forEach((filePath) => {
          const partialPath = module.exports.normalizePath(filePath, targetDir, false);
          const sourcePath = path.join(sourceDir, partialPath);
          try {
            fs.statSync(sourcePath);
          } catch (er) {
            result.push(path.join(targetDir, partialPath));
          }
        });
        resolve(result);
      });
    });
  };

  module.exports.cleanDir = function(logLabel, targetPath, basePath) {
    const fs = require('fs');
    const path = require('path');
    const resolvedPath = path.resolve(targetPath);
    try {
      const stat = fs.statSync(resolvedPath);
      if (!stat.isDirectory()) return;
      fs.readdirSync(resolvedPath).forEach((child) => {
        module.exports.cleanDir(logLabel, path.join(resolvedPath, child), basePath);
      });
      if (fs.readdirSync(resolvedPath).length <= 0) {
        fs.rmdirSync(resolvedPath);
        module.exports.log(logLabel, resolvedPath, basePath);
      }
    } catch (er) {
    }
  };

  module.exports.notify = function(task, plugin, message) {
    const notifier = require('node-notifier');
    notifier.notify({
      title: `build-tools / ${task} / ${plugin}`,
      message: message
    });
  };
})();
