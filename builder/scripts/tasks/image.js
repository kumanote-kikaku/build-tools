(function() {
  'use strict';

  const TASK = 'image';

  module.exports = (gulp, setting) => {
    const util = require('../util');

    const directory = setting.directory;
    const taskSetting = setting.task[TASK];
    const watchSetting = setting.watch || {};

    const srcPattern = util.getFilePattern(directory.src, '', taskSetting.exts);
    const cachePattern = util.getFilePattern(directory.cache, '', taskSetting.exts);

    function clean(cb) {
      util.getOldFiles(directory.cache, directory.src).then((paths) => {
        const del = require('del');
        if (paths.length > 0) {
          del(paths).then((paths) => {
            paths.forEach((path) => {
              util.log('clean-image', path, directory.cache, 'magenta');
            });
            util.cleanDir('clean-dir-image', directory.cache);
            cb();
          });
        } else {
          util.cleanDir('clean-dir-image', directory.cache, directory.cache);
          cb();
        }
      });
    }

    function cache() {
      const plumber = require('gulp-plumber');
      const imagemin = require('gulp-imagemin');
      const logger = require('../plugins/logger');
      const changed = require('gulp-changed');

      return gulp.src([srcPattern])
        .pipe(plumber())
        .pipe(changed(directory.cache))
        .pipe(imagemin({
          interlaced: true,
          progressive: true
        }))
        .pipe(gulp.dest(directory.cache))
        .pipe(logger(`cache-${TASK}`, directory.cache, 'white'));
    }

    function build(all) {
      const plumber = require('gulp-plumber');
      const logger = require('../plugins/logger');
      const livereload = require('gulp-livereload');

      let stream = gulp.src([cachePattern]).pipe(plumber());

      if (!all) {
        const changed = require('gulp-changed');
        stream = stream.pipe(changed(directory.release));
      }

      return stream
        .pipe(gulp.dest(directory.preview))
        .pipe(gulp.dest(directory.release))
        .pipe(logger(TASK, directory.release, 'green'))
        .pipe(livereload());
    }

    /* -------------------------------------------------------------------------- */

    gulp.task(`clean-${TASK}`, (cb) => {
      clean(cb);
    });

    gulp.task(`cache-${TASK}`, [`clean-${TASK}`], () => {
      return cache();
    });

    gulp.task(`prepare-${TASK}`, [`cache-${TASK}`], (cb) => {
      cb();
    });

    gulp.task(`build-${TASK}`, () => {
      return build();
    });

    gulp.task(`build-all-${TASK}`, () => {
      return build(true);
    });

    gulp.task(`watch-${TASK}`, (cb) => {
      gulp.watch([srcPattern], watchSetting, [`prepare-${TASK}`]);
      gulp.watch([cachePattern], watchSetting, [`build-${TASK}`]);
      cb();
    });
  };

})();
