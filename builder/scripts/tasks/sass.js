(function() {
  'use strict';

  const TASK = 'sass';

  module.exports = (gulp, setting) => {
    const util = require('../util');

    const directory = setting.directory;
    const taskSetting = setting.task[TASK];
    const watchSetting = setting.watch || {};

    const srcPattern = util.getFilePattern(directory.src, '', taskSetting.exts);
    const libPattern = util.getFilePattern(directory.src, taskSetting.libraryPrefix, taskSetting.exts);

    function build(all) {
      const plumber = require('gulp-plumber');
      const sass = require('gulp-sass');
      const autoprefixer = require('gulp-autoprefixer');
      const combineMq = require('gulp-combine-mq');
      const csscomb = require('gulp-csscomb');
      const replace = require('gulp-replace');
      const logger = require('../plugins/logger');
      const livereload = require('gulp-livereload');

      let stream = gulp.src([srcPattern, `!${libPattern}`]).pipe(plumber((er) => {
        util.notify(TASK, er.plugin, er.message);
      }));

      if (!all) {
        const changed = require('gulp-changed');
        stream = stream.pipe(changed(directory.release, {extension: '.css'}));
      }

      stream = stream
        .pipe(sass({
          includePaths: ['files/sass', directory.src]
        }).on('error', sass.logError));

      if (taskSetting.combineMq) {
        stream = stream.pipe(combineMq());
      }

      return stream
        .pipe(autoprefixer({browsers: taskSetting.browsers}))
        .pipe(csscomb())
        .pipe(replace(/[ ]+$/mg, ''))
        .pipe(replace(/@charset\s+'UTF-8'/g, '@charset "UTF-8"'))
        .pipe(gulp.dest(directory.preview))
        .pipe(gulp.dest(directory.release))
        .pipe(logger(TASK, directory.release, 'red'))
        .pipe(livereload());
    }

    /* -------------------------------------------------------------------------- */

    gulp.task(`prepare-${TASK}`, (cb) => { cb(); });

    gulp.task(`build-${TASK}`, () => {
      return build();
    });

    gulp.task(`build-all-${TASK}`, () => {
      return build(true);
    });

    gulp.task(`watch-${TASK}`, (cb) => {
      gulp.watch([srcPattern, `!${libPattern}`], watchSetting, [`build-${TASK}`]);
      gulp.watch([libPattern], watchSetting, [`build-all-${TASK}`]);
      cb();
    });
  };

})();
