(function() {
  'use strict';

  const TASK = 'copy';

  module.exports = (gulp, setting) => {
    const util = require('../util');

    const directory = setting.directory;
    const taskSetting = setting.task[TASK];
    const watchSetting = setting.watch || {};

    const srcPattern = util.getFilePattern(directory.src, '', taskSetting.exts);

    function build(all) {
      const plumber = require('gulp-plumber');
      const logger = require('../plugins/logger');
      const livereload = require('gulp-livereload');

      let stream = gulp.src([srcPattern]).pipe(plumber());

      if (!all) {
        const changed = require('gulp-changed');
        stream = stream.pipe(changed(directory.release));
      }

      return stream
        .pipe(gulp.dest(directory.preview))
        .pipe(gulp.dest(directory.release))
        .pipe(logger(TASK, directory.release, 'green'))
        .pipe(livereload());
    }

    /* -------------------------------------------------------------------------- */

    gulp.task(`prepare-${TASK}`, (cb) => { cb(); });

    gulp.task(`build-${TASK}`, () => {
      return build();
    });

    gulp.task(`build-all-${TASK}`, () => {
      return build(true);
    });

    gulp.task(`watch-${TASK}`, (cb) => {
      gulp.watch([srcPattern], watchSetting, [`build-${TASK}`]);
      cb();
    });
  };

})();
