(function() {
  'use strict';

  const TASK = 'serve-node';

  module.exports = (gulp, setting) => {
    const util = require('../util');

    const directory = setting.directory;
    const serveSetting = setting.serve;
    const middleware = [];

    function guessContentType(req, res, next) {
      var type = null;
      if (/^\.html$/.test(req.url) ||
          /\/$/.test(req.url)) {
        type = 'text/html';
      } else if (/^\.css$/.test(req.url)) {
        type = 'text/css';
      } else if (/^\.js$/.test(req.url)) {
        type = 'text/javascript';
      }
      if (type !== null) {
        res.setHeader('Content-Type', type + ';charset=UTF-8');
      }
      next();
    }

    middleware.push(guessContentType);
    if (serveSetting.ssi) {
      const connectSSI = require('connect-ssi');
      middleware.push(connectSSI({
        baseDir: serveSetting.root || directory.preview,
        ext: '.html'
      }));
    }

    gulp.task(TASK, ['watch'], () => {
      const plumber = require('gulp-plumber');
      const livereload = require('gulp-livereload');
      const webserver = require('gulp-webserver');

      livereload.listen();
      util.notify(TASK, 'gulp-webserver', 'start server');
      return gulp.src(serveSetting.root || directory.preview)
        .pipe(plumber())
        .pipe(webserver({
          host: serveSetting.host,
          port: serveSetting.port,
          middleware: middleware
        }));
    });

    gulp.task('default', [TASK]);
  };
})();
