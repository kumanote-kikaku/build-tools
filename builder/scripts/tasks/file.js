(function () {
  'use strict';

  const TASK = 'file';

  module.exports = (gulp, setting) => {
    const util = require('../util');

    const directory = setting.directory;
    const taskSetting = setting.task[TASK];
    const watchSetting = setting.watch || {};

    const srcPattern = util.getFilePattern(directory.src, '', taskSetting.exts);
    const libPattern = util.getFilePattern(directory.src, taskSetting.libraryPrefix, taskSetting.exts);

    function build(all) {
      const plumber = require('gulp-plumber');
      const replace = require('gulp-replace');
      const include = require('gulp-file-include');
      const commentReplace = require('../plugins/comment-replace');
      const logger = require('../plugins/logger');
      const livereload = require('gulp-livereload');

      let stream = gulp.src([srcPattern, `!${libPattern}`]).pipe(plumber((er) => {
        util.notify(TASK, er.plugin, er.message);
      }));

      if (!all) {
        const changed = require('gulp-changed');
        stream = stream.pipe(changed(directory.release));
      }

      return stream
        .pipe(replace(/<!--(\$\{include(.|\x0a|\x0d)*?\})-->/g, '$1'))
        .pipe(include({
          prefix: '\\$\\{',
          suffix: '\\}',
          basepath: directory.src,
          context: taskSetting.appendix
        }))
        .pipe(gulp.dest(directory.preview))
        .pipe(commentReplace())
        .pipe(gulp.dest(directory.release))
        .pipe(logger(TASK, directory.release, 'yellow'))
        .pipe(livereload());
    }

    /* -------------------------------------------------------------------------- */

    gulp.task(`prepare-${TASK}`, (cb) => { cb(); });

    gulp.task(`build-${TASK}`, () => {
      return build();
    });

    gulp.task(`build-all-${TASK}`, () => {
      return build(true);
    });

    gulp.task(`watch-${TASK}`, (cb) => {
      gulp.watch([srcPattern, `!${libPattern}`], watchSetting, [`build-${TASK}`]);
      gulp.watch([libPattern], watchSetting, [`build-all-${TASK}`]);
      cb();
    });
  };

})();
