(function() {
  'use strict';

  /* eslint no-console: "off" */

  module.exports = (gulp, setting) => {
    const util = require('../util');
    const tasks = Object.keys(setting.task);

    gulp.task('clean', (cb) => {
      const del = require('del');
      del([setting.directory.preview, setting.directory.release]).then((paths) => {
        paths.forEach((path) => {
          util.log('clean', path, 'sites', 'magenta');
        });
        cb();
      }, (err) => {
        console.error(err.message);
      });
    });

    gulp.task('prepare', ['clean'], (cb) => {
      const runSequence = require('run-sequence').use(gulp);
      runSequence(tasks.map((x) => { return `prepare-${x}`; }), cb);
    });

    gulp.task('build', ['prepare'], (cb) => {
      const runSequence = require('run-sequence').use(gulp);
      runSequence(tasks.map((x) => { return `build-${x}`; }), cb);
    });

    gulp.task('watch', ['build'], (cb) => {
      const runSequence = require('run-sequence').use(gulp);
      runSequence.call(null, tasks.map((x) => { return `watch-${x}`; }), cb);
    });

    gulp.task('default', ['watch']);
  };
})();
