(function() {
  'use strict';

  const TASK = 'no-serve';

  module.exports = (gulp, setting) => {
    gulp.task(TASK, ['watch'], () => {
      const livereload = require('gulp-livereload');
      livereload.listen();
    });

    gulp.task('default', [TASK]);
  };
})();
