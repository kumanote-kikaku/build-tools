/*globals process*/
(function() {
  'use strict';

  const TASK = 'serve-php';

  module.exports = (gulp, setting) => {
    const util = require('../util');

    const directory = setting.directory;
    const serveSetting = setting.serve;

    function expandEnv(value) {
      return value.replace(/\$\{([^\}]+)\}/g, function(all, name) {
        return process.env[name] || all;
      });
    }

    function findPhpExecutable(list) {
      const fs = require('fs');
      const path = require('path');

      return new Promise((resolve, reject) => {
        Array.from(list).some((executable) => {
          const executablePath = path.resolve(expandEnv(executable));
          try {
            fs.statSync(executablePath);
            resolve(executablePath);
            return true;
          } catch (er) {
          }
          return false;
        });
        reject(new Error('php executable not found.'));
      });
    }

    function findPhpIni(object) {
      const fs = require('fs');
      const path = require('path');
      const list = [];

      if (process.platform in object) {
        [].push.apply(list, object[process.platform]);
      }
      if ('default' in object) {
        [].push.apply(list, object.default);
      }
      return new Promise((resolve, reject) => {
        Array.from(list).some((ini) => {
          const iniPath = path.resolve(ini);
          try {
            fs.statSync(iniPath);
            resolve(iniPath);
            return true;
          } catch (er) {
          }
          return false;
        });
        reject(new Error('php.ini not found.'));
      });
    }

    function findRouter(router) {
      if (typeof router === 'string') {
        const fs = require('fs');
        const path = require('path');
        const routerPath = path.resolve(router);
        try {
          fs.statSync(routerPath);
          return routerPath;
        } catch (er) {
        }
      }
      return null;
    }

    gulp.task(TASK, ['watch'], (cb) => {
      const livereload = require('gulp-livereload');
      const connectPhp = require('gulp-connect-php');

      findPhpExecutable(serveSetting.phpExecutable).then((executable) => {
        util.logMessage(TASK, `use ${executable}`, 'blue');
        findPhpIni(serveSetting.phpIni).then((ini) => {
          util.logMessage(TASK, `use ${ini}`, 'blue');
          const router = findRouter(serveSetting.router);
          const options = {
            base: serveSetting.root || directory.preview,
            hostname: serveSetting.host,
            port: serveSetting.port,
            bin: executable,
            ini: ini
          };
          if (router !== null) {
            util.logMessage(TASK, `use ${router}`, 'blue');
            options.router = router;
          }
          livereload.listen();
          connectPhp.server(options, () => {
            cb();
            util.notify(TASK, 'gulp-connect-php', 'start server');
            cb = function() {};
          });
        }, cb);
      }, cb);
    });

    gulp.task('default', [TASK]);
  };
})();
