(function() {
  'use strict';

  const TASK = 'js';

  module.exports = (gulp, setting) => {
    const util = require('../util');

    const directory = setting.directory;
    const taskSetting = setting.task[TASK];
    const watchSetting = setting.watch || {};

    const srcPattern = util.getFilePattern(directory.src, '', taskSetting.exts);
    const ignorePattern = util.getFilePattern(directory.src, '', taskSetting.ignoreExts);

    function build(all) {
      const plumber = require('gulp-plumber');
      const commentReplace = require('../plugins/comment-replace');
      const stripComments = require('gulp-strip-comments');
      const prettify = require('gulp-js-prettify');
      const footer = require('gulp-footer');
      const logger = require('../plugins/logger');
      const livereload = require('gulp-livereload');

      let stream = gulp.src([srcPattern, `!${ignorePattern}`]).pipe(plumber());

      if (!all) {
        const changed = require('gulp-changed');
        stream = stream.pipe(changed(directory.release));
      }

      return stream
        .pipe(gulp.dest(directory.preview))
        .pipe(commentReplace())
        .pipe(stripComments({
          safe: true
        }))
        .pipe(prettify({
          indent_size: 2
        }))
        .pipe(footer('\n'))
        .pipe(gulp.dest(directory.release))
        .pipe(logger(TASK, directory.release, 'cyan'))
        .pipe(livereload());
    }

    /* -------------------------------------------------------------------------- */

    gulp.task(`prepare-${TASK}`, (cb) => { cb(); });

    gulp.task(`build-${TASK}`, () => {
      return build();
    });

    gulp.task(`build-all-${TASK}`, () => {
      return build(true);
    });

    gulp.task(`watch-${TASK}`, (cb) => {
      gulp.watch([srcPattern, `!${ignorePattern}`], watchSetting, [`build-${TASK}`]);
      cb();
    });
  };

})();
